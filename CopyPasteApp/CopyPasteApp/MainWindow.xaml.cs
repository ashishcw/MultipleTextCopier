﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace CopyPasteApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        private bool appRunning = false;
        private System.Windows.Threading.DispatcherTimer timer;
        private string[] AllStrings = new string[10];
        //private List<String> AllStrings;
        private LowLevelKeyboardListener _listener;
        
        string temptext;

        public MainWindow()
        {
            InitializeComponent();
            timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(timer_tick);
            
            //AllStrings = new List<string>();
            timer.Interval = new TimeSpan(0, 0, 10);
            ApplicationLoading();
            temptext = System.Windows.Clipboard.GetText();
                       
            
        }


        private void ApplicationLoading()
        {
            _listener = new LowLevelKeyboardListener();
            _listener.OnKeyPressed += _listener_OnKeyPressed;

            _listener.HookKeyboard();
        }

        private void _listener_OnKeyPressed(object sender, KeyPressedArgs e)
        {
            _listener.UnHookKeyboard();
            _listener.HookKeyboard();
            if (timer.IsEnabled)
            {
                if (e.KeyPressed == Key.F1 && AllStrings[0] == null)
                {
                    if(System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[0] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                }
                if (e.KeyPressed == Key.F2 && AllStrings[1] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[1] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                        
                }
                if (e.KeyPressed == Key.F3 && AllStrings[2] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[2] = temptext;
                        System.Windows.Clipboard.Clear();
                    }

                }
                if (e.KeyPressed == Key.F4 && AllStrings[3] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[3] = temptext;
                        System.Windows.Clipboard.Clear();
                    }                   
                }
                if (e.KeyPressed == Key.F5 && AllStrings[4] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[4] = temptext;
                        System.Windows.Clipboard.Clear();
                    }                   
                }
                if (e.KeyPressed == Key.F6 && AllStrings[5] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[5] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                  
                }
                if (e.KeyPressed == Key.F7 && AllStrings[6] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[6] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                  
                }
                if (e.KeyPressed == Key.F8 && AllStrings[7] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[7] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                   
                }
                if (e.KeyPressed == Key.F9 && AllStrings[8] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[8] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                    
                }
                if (e.KeyPressed == Key.F10 && AllStrings[9] == null)
                {
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        temptext = System.Windows.Clipboard.GetText();
                        AllStrings[9] = temptext;
                        System.Windows.Clipboard.Clear();
                    }
                   
                }


                //Keys Assigned to the Copy Texts
                if (e.KeyPressed == Key.F1)
                {
                    if (AllStrings[0] != null)
                    {
                        System.Windows.Clipboard.SetText(AllStrings[0]);
                        if ((bool)checkBox.IsChecked)
                        {
                            System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());                            
                        }
                        
                    }
                    
                    //System.Windows.Forms.SendKeys.Send(AllStrings[0]);
                }


                if (e.KeyPressed == Key.F2 && AllStrings[1] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[1]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F3 && AllStrings[2] != null)
                {
                    //System.Windows.Forms.SendKeys.Send(AllStrings[2]);
                    System.Windows.Clipboard.SetText(AllStrings[2]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F4 && AllStrings[3] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[3]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F5 && AllStrings[4] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[4]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F6 && AllStrings[5] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[5]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F7 && AllStrings[6] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[6]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F8 && AllStrings[7] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[7]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F9 && AllStrings[8] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[8]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }
                if (e.KeyPressed == Key.F10 && AllStrings[9] != null)
                {
                    System.Windows.Clipboard.SetText(AllStrings[9]);
                    if ((bool)checkBox.IsChecked)
                    {
                        System.Windows.Forms.SendKeys.SendWait(System.Windows.Clipboard.GetText());
                    }
                }



                //UI Text Reflect in UI Texbox
                textBox.Text = AllStrings[0];
                textBox_Copy.Text = AllStrings[1];
                textBox_Copy1.Text = AllStrings[2];
                textBox_Copy2.Text = AllStrings[3];
                textBox_Copy3.Text = AllStrings[4];
                textBox_Copy4.Text = AllStrings[5];
                textBox_Copy5.Text = AllStrings[6];
                textBox_Copy6.Text = AllStrings[7];
                textBox_Copy7.Text = AllStrings[8];
                textBox_Copy8.Text = AllStrings[9];
                

            }


        }


        private void timer_tick(object sender, EventArgs e)
        {
            //System.Windows.MessageBox.Show("KeyPressedEvenArguments");
          
        }

      
        private void button_Click(object sender, RoutedEventArgs e) // Start Button
        {
            appRunning = !appRunning;

            if (!appRunning)
            {
                button.Content = "Start";
                timer.Stop();

            }
            else
            {
                button.Content = "Stop";
                timer.Start();
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e) //1st textbox
        {
            //if (!timer.IsEnabled)
            
        }

        private void textBox_Copy_TextChanged(object sender, TextChangedEventArgs e) //2nd TextBox
        {
            //if (timer.IsEnabled)
           
        }

        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e) //3rd TextBox
        {
            //if (timer.IsEnabled)
           
        }

        private void textBox_Copy2_TextChanged(object sender, TextChangedEventArgs e) //4th TextBox
        {
            //if (timer.IsEnabled)
           
        }

        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e) //5th TextBox
        {
            //if (timer.IsEnabled)
            
        }

        private void textBox_Copy4_TextChanged(object sender, TextChangedEventArgs e) //6th TextBox
        {
            //if (timer.IsEnabled)
          
        }

        private void textBox_Copy5_TextChanged(object sender, TextChangedEventArgs e) //7th TextBox
        {
            //if (timer.IsEnabled)
           
        }

        private void textBox_Copy6_TextChanged(object sender, TextChangedEventArgs e) //8th TextBox
        {
            //if (timer.IsEnabled)
           
        }

        private void textBox_Copy7_TextChanged(object sender, TextChangedEventArgs e) //9th TextBox
        {
            //if (timer.IsEnabled)
            
        }     

        private void textBox_Copy8_TextChanged(object sender, TextChangedEventArgs e) //10th TextBox
        {
            //if (timer.IsEnabled)
            
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            ClearFunction();
        }

        private void ClearFunction()
        {
            for (int i = 0; i < AllStrings.Length; i++)
            {
                if (AllStrings[i] != null)
                {
                    AllStrings[i] = null;
                    //AllStrings[i] = "";
                }               
            }

            textBox.Text = "";
            textBox_Copy.Text = "";
            textBox_Copy1.Text = "";
            textBox_Copy2.Text = "";
            textBox_Copy3.Text = "";
            textBox_Copy4.Text = "";
            textBox_Copy5.Text = "";
            textBox_Copy6.Text = "";
            textBox_Copy7.Text = "";
            textBox_Copy8.Text = "";
            System.Windows.Clipboard.Clear();
        }
    }
}
